const express = require('express')

exports.createRouter = (userControllers) => {
    const router = express.Router()
    router.get('/user', (req, res, next) => {
        userControllers.retrieve(req, res, next)
    })
    router.post('/user', (req, res, next) => {
        userControllers.create(req, res, next)
    })
    router.get('/user/:userId', (req, res, next) => {
        userControllers.get(req, res, next)
    })
    router.put('/user/:userId', (req, res, next) => {
        userControllers.update(req, res, next)
    })
    router.delete('/user/:userId', (req, res, next) => {
        userControllers.delete(req, res, next)
    })
    return router
}
