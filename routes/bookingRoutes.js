const express = require('express')

exports.createRouter = (bookingControllers) => {
    const router = express.Router()
    router.get('/booking', (req, res, next) => {        
        bookingControllers.retrieve(req, res, next)
    })
    router.post('/booking', (req, res, next) => {
        bookingControllers.create(req, res, next)
    })
    router.get('/booking/:bookingId', (req, res, next) => {
        bookingControllers.get(req, res, next)
    })
    router.put('/booking/:bookingId', (req, res, next) => {
        bookingControllers.update(req, res, next)
    })
    router.patch('/booking/:bookingId/accept', (req, res, next) => {
        if (typeof req.user.keeperData === "object")
            bookingControllers.accept(req, res, next)
    })
    router.delete('/booking/:bookingId', (req, res, next) => {
        bookingControllers.delete(req, res, next)
    })
    router.delete('booking/:bookingId/cancel', (req, res, next) => {
        bookingControllers.cancel(req, res, next)
    })
    return router
}
