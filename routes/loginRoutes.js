const express = require('express')

exports.createRouter = (loginControllers) => {
    const router = express.Router()
    router.post('/login', (req, res, next) => {
        loginControllers.login(req, res, next)
    })
    router.post('/logout', (req, res, next) => {
        loginControllers.logout(req, res, next)
    })
    router.post('/signin', (req, res, next) => {
        loginControllers.signin(req, res, next)
    })
    return router
}
