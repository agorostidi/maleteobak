const userRouter = require('./userRoutes')
const bookingRouter = require('./bookingRoutes')
const express = require('express')

exports.createRouter = (controllers, middlewares) => {
    const toReturn = express.Router()
    toReturn.use(middlewares.authenticationMiddleware.middleware)
    toReturn.use(userRouter.createRouter(controllers.user))
    toReturn.use(bookingRouter.createRouter(controllers.booking))
    return toReturn
}
