const schemas = require('./schemas')
const routes = require('./routes')
const controllers = require('./controllers')
const services = require('./services')
const mongoose = require('mongoose')
const express = require('express')
const bodyParser = require('body-parser')
const middlewares = require('./middlewares')
const passport    = require('passport');
const cors = require('cors')

require('dotenv').config();
require('./utils/passport');
mongoose.connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
 
   if (err) throw err;
 
   console.log(`Successfully connected with ${process.env.DB}`);
 
});

const models = {}
models.User = mongoose.model('User', schemas.schemas.userSchema.default)
models.Booking = mongoose.model('Booking', schemas.schemas.bookingSchema.default)

/*
var testUser= new models.User ({

  name: "Andres",
  surname: "Gorostidi",
  email: "andres.gorostidi@test.com"
})

testUser.save();
*/

const serv = services.createService(models, mongoose)
const contr = controllers.createController(serv)
const authenticationRouter = require('./routes/loginRoutes').createRouter(contr.login);
const businessRouter = routes.createRouter(contr, middlewares)
const app = express()

app.use(cors());   // Enable all Cors request
app.use(bodyParser.json({ type: 'application/json' }))
app.use(authenticationRouter)
app.use(businessRouter)

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
  });