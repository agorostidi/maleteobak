exports.createService = (Booking, mongoose) => {
    const service = {}
    service.retrieve = (async (userId, asKeeper = false) => {
        try {
            var documents = {};
            if (asKeeper)
                documents = await Booking.find({
                    keeper: userId,
                    deletedAt: null
                })
            else
                documents = await Booking.find({
                    client: userId,
                    deletedAt: null
                })
            return documents
        } catch (error) {
            return null
        }
    })
    service.create = (async (booking) => {
        const bookingToCreate = new Booking(booking)
        try {
            const document = await bookingToCreate.save()
            return document
        } catch (error) {
            return null
        }
    })
    service.read = (async (bookingId, userId, asKeeper = false) => {
        try {
            const document = await Booking.findById(bookingId)
            if (
                (asKeeper && document.keeper === userId) ||
                (!asKeeper && document.client === userId))
                return document
            else return null
        } catch (error) {
            return null
        }
    })
    service.update = (async (bookingId, booking, asKeeper = false) => {
        try {
            const bookingToUpdate = await Booking.findById(bookingId)
            // Comprobamos si no tiene permiso para actualizar, en cuyo caso devolvemos null y no actualizamos el documento
            if (!(
                (asKeeper && document.keeper === userId) ||
                (!asKeeper && document.client === userId)))
                return null
            Object.keys(booking).forEach((key) => {
                bookingToUpdate[key] = booking[key]
            })
            const document = await bookingToUpdate.save()
            return document
        } catch (error) {
            return null
        }
    })
    service.delete = (async (bookingId,userId, asKeeper = false) => {
        try {
            const bookingToUpdate = await Booking.findById(bookingId)
            bookingToUpdate.deletedAt = Date.now()
            bookingToUpdate.deletedBy = userId
            bookingToUpdate.cancel = asKeeper
            const document = await bookingToUpdate.save()
            return document
        } catch (error) {
            return null
        }
    })
    return service
}
