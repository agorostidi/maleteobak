exports.createService = (User, mongoose) => {
    const service = {}
    service.retrieve = (async (email, password) => {
        try {
            
            const where = {
                deletedAt: null
            };

            if (email !== null){
                where.email = email;
            }

            if (password !== null){
                where.passwordHash = password;
            }

            const documents = await User.find(where)
            return documents
        } catch (error) {
            return null
        }
    })
    service.create = (async (user) => {
        const userToCreate = new User(user)
        try {
            const document = await userToCreate.save()
            return document
        } catch (error) {
            return null
        }
    })
    service.read = (async (userId) => {
        try {
            const document = await User.findById(userId)
            return document
        } catch (error) {
            return null
        }
    })
    service.update = (async (userId, user) => {
        try {
            const userToUpdate = await User.findById(userId)
            Object.keys(user).forEach((key) => {
                userToUpdate[key] = user[key]
            })
            const document = await userToUpdate.save()
            return document
        } catch (error) {
            return null
        }
    })
    service.delete = (async (userId) => {
        try {
            const userToUpdate = await User.findById(userId)
            userToUpdate.deletedAt = Date.now()
            const document = await userToUpdate.save()
            return document
        } catch (error) {
            return null
        }
    })
    return service
}
