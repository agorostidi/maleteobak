const userService = require('./userService')
const bookingService = require('./bookingService')


exports.createService = (schemas, connection) => {
    return {
        booking: bookingService.createService(schemas.booking, connection),
        user: userService.createService(schemas.User, connection)
    }
}
