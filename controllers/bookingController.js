exports.createController = (bookingService) => {
    const controller = {}

    controller.retrieve = (req,res,next) => {
        const userId = req.user.userId
        const asKeeper = req.query.keeper || false
        bookingService.retrieve(userId, false).then((documents) => {
            res.status(200).send(documents)
        }).catch(()=> res.status)
    }
    controller.create = (req,res,next) => {
        const booking = req.body
        booking.client = req.user.id
        bookingService.create(booking).then((response) => {
            if (response) {
                res.status(201).send(response)
            } else {
                next({
                    httpError: 503,
                    description: 'DB error'
                })
            }
        })
        
    }
    controller.get = (req,res,next) => {
        const userId = req.user.userId
        const bookingId = req.params.bookingId
        userService.read(bookingId, userId).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'Booking does not exists'
                })
            }
        })
    }
    controller.update = (req,res,next) => {
        const userId = req.user.userId
        const bookingId = req.params.bookingId
        const booking = req.body
        userService.update(bookingId, userId, booking).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'Booking does not exists'
                })
            }
        }) 
    }
    controller.accept = (req,res,next) => {
        const userId = req.user.userId
        const bookingId = req.params.bookingId
        const booking = {
            accepted : true
        }
        userService.update(bookingId, userId, booking, true).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'Booking does not exists'
                })
            }
        }) 
        
    }
    controller.delete = (req,res,next) => {
        const userId = req.user.userId
        const bookingId = req.params.bookingId
        userService.read(bookingId, userId).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'Booking does not exists'
                })
            }
        })   
    }
    controller.cancel = (req,res,next) => {
        const userId = req.user.userId
        const bookingId = req.params.bookingId
        userService.read(bookingId, userId, true).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'Booking does not exists'
                })
            }
        })   
    }
    return bookingService
}