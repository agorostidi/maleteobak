const userControllerCreator = require('./userController');
const passport = require('passport');
userController = userControllerCreator.createController();
const jwt = require('jsonwebtoken');


exports.createController = (userService) => {
    const controller = {}

    controller.login = ((req, res, next) => {
        passport.authenticate('local', {session: false}, (err, user, info) => {
            if (err || !user) {
                return res.status(400).json({
                    message: 'Something is not right',
                    user   : user
                });
            }
           req.login(user, {session: false}, (err) => {
               if (err) {
                   res.send(err);
               }
               // genersate a signed son web token with the contents of user object and return it in the response
               const toSign = {
                   id: user.id,
                   email: user.email
               }
               const token = jwt.sign(toSign, 'your_jwt_secret');
               return res.json({toSign, token});
            });
        })(req, res)
    })
    controller.logout = ((req, res, next) => {
        
    })

    controller.signin = userController.create

    return controller
}
