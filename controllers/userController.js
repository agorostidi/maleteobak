exports.createController = (userService) => {
    const controller = {}

    controller.retrieve = ((req, res, next) => {
        userService.retrieve().then((documents) => {
            res.status(200).send(documents)
        })
    })
    controller.create = ((req, res, next) => {
        const user = req.body
        if (user.name === '' || user.name === undefined) {
            return next({
                httpError: 400,
                description: 'User must have a name'
            })
        }
        if (user.password === '' || user.password === undefined) {
            return next({
                httpError: 400,
                description: 'User must have a password'
            })
        }
        // eslint-disable-next-line no-undef
        user.passwordHash = user.password
        if (user.surname === '' || user.surname === undefined) {
            return next({
                httpError: 400,
                description: 'User must have a surname'
            })
        }
        if (user.email === '' || user.email === undefined) {
            return next({
                httpError: 400,
                description: 'User must have a email'
            })
        }
        userService.create(user).then((response) => {
            if (response) {
                res.status(201).send(response)
            } else {
                next({
                    httpError: 503,
                    description: 'DB error'
                })
            }
        })
    })
    controller.get = ((req, res, next) => {
        const userId = req.params.userId
        userService.read(userId).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'User does not exists'
                })
            }
        })
    })
    controller.update = ((req, res, next) => {
        const userId = req.params.userId
        const user = req.body
        userService.update(userId, user).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'User does not exists'
                })
            }
        })
    })
    controller.delete = ((req, res, next) => {
        const userId = req.params.userId
        userService.delete(userId).then((response) => {
            if (response) {
                res.status(200).send(response)
            } else {
                next({
                    httpError: 404,
                    description: 'User does not exists'
                })
            }
        })
    })

    return controller
}
