const userController = require('./userController')
const bookingController = require('./bookingController')
const loginController = require('./loginController')

exports.createController = (services) => {
    return {
        booking: bookingController.createController(services.booking),
        user: userController.createController(services.user),
        login: loginController.createController(services.user)
    }
}
