const passport = require('passport')

exports.middleware = passport.authenticate('jwt', {session: false});