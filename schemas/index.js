//const bookingSchema from './bookingSchema'
const userSchema = require('./userSchema')
const bookingSchema = require('./bookingSchema')


exports.schemas = {
    userSchema: userSchema,
    bookingSchema: bookingSchema
};
// booking: bookingSchema,  });
