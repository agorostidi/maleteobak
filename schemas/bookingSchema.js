const mongoose = require('mongoose')

const bookingSchema = new mongoose.Schema({
    startDate: Date,
    endDate: Date,
    firstDayPrice: Number,
    extraDayPrice: Number,
    suitcasesPieces: Number,
    client: mongoose.ObjectId,
    keeper: mongoose.ObjectId,
    accepted: Boolean,
    canceled: Boolean,
    deletedBy: mongoose.ObjectId,
    createdAt: Date,
    deletedAt: Date
})

exports.default = bookingSchema
