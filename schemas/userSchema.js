const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: String,
    surname: String,
    email: String,
    passwordHash: String,
    keeperData: Map,
    createdAt: Date,
    deletedAt: Date
})

exports.default = userSchema
