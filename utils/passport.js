const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require("passport-jwt");
const mongoose = require('mongoose')
const schemas = require('../schemas')
const userModel = mongoose.model('User', schemas.schemas.userSchema.default)
const userService = require('../services/userService').createService(userModel);
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, 
function (email, password, cb) {
  //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
  return userService.retrieve(email, password).then(users => {
         if (users.length !== 1) {
             return cb(null, false, {message: 'Incorrect email or password.'});
         }
         return cb(null, users[0], {message: 'Logged In Successfully'});
    })
    .catch(err => cb(err));
}))

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey   : 'your_jwt_secret'
},
function (jwtPayload, cb) {

  //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
  return userService.read(jwtPayload.id)
      .then(user => {
          return cb(null, user);
      })
      .catch(err => {
          return cb(err);
      });
}
));